/*
 * Copyright (c) 2015-2016, Thomas Keh
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. Neither the name of the copyright holder nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "bridge.h"
#include "logger.h"
#include "joint_state_publisher.h"
#include "joint_state_subscriber.h"
#include "entry_publisher.h"
#include "entry_subscriber.h"

#include <thread>
#include <chrono>
#include <memory>

int main(int argc, char* argv[]) {

	const std::string busname = "can0";
	const std::string baudrate = "500k";
	const double loop_rate = 10;

	kaco::Master master;
	if(!master.start(busname, baudrate)){
		ERROR("Start master failed.");
		return EXIT_FAILURE;
	}
	
	std::this_thread::sleep_for(std::chrono::seconds(1));
	size_t num_devices_required = 2;
	while(master.num_devices() < num_devices_required){
		ERROR("Number of devices found: " << master.num_devices() << ". Waiting for " << num_devices_required << ".");
		master.core.nmt.discover_nodes();
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}

	// Create bridge
	ros::init(argc, argv, "canopen_bridge");
	kaco::Bridge bridge;

	PRINT("Num Devices: " << master.num_devices());
	bool devices_found = false;
	for(size_t i =0; i < master.num_devices(); ++i){
		kaco::Device& device = master.get_device(i);
		device.start();

		device.load_dictionary_from_library();
		const auto profile = device.get_device_profile_number();

		if(profile==402){
			devices_found = true;
			// Enable Operation
			device.execute("Enable operation");

			auto jspub = std::make_shared<kaco::JointStatePublisher>(device, 0, 350000);
			bridge.add_publisher(jspub, loop_rate);
	
			auto jssub = std::make_shared<kaco::JointStateSubscriber>(device, 0, 350000);
			bridge.add_subscriber(jssub);
		}
	}
		
	bridge.run();
}
