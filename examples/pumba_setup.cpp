/*
 * Copyright (c) 2015-2016, Thomas Keh
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 *    3. Neither the name of the copyright holder nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
 
#include <chrono>
#include <vector>
#include <iostream>
#include <thread>
#include <cstdint>

#include "core.h"
#include "master.h"
#include "logger.h"
#include "canopen_error.h" 

int main() {

	// ----------- //
	// Preferences //
	// ----------- //

	// The node ID of the slave we want to communicate with.
	const uint8_t node_id = 2;

	// Set the name of your CAN bus. "slcan0" is a common bus name
	// for the first SocketCAN device on a Linux system.
	const std::string busname = "can0";

	const std::string baudrate = "500K";

	// Alternative: CiA-402 (motor) control word:
	const uint16_t ind_save = 0x1010;
	const uint16_t ind_hb	= 0x1017;
	// Sub-Index
	const uint8_t sub_0 = 0x00;
	const uint8_t sub_1 = 0x01;
	const uint8_t sub_2 = 0x02;
	const uint8_t sub_3 = 0x03;
	const uint8_t sub_4 = 0x04;
	const uint8_t sub_5 = 0x05;

	// --------------------------------------------------------------------------------------------------------------
	// Core Initialization //
	// ------------------- //
	// Create core.
	kaco::Core core;
	// This will be set to true by the callback below.
	bool found_node = false;

	std::cout << "Registering a callback which is called when a device is detected via NMT..." << std::endl;
	core.nmt.register_device_alive_callback([&] (const uint8_t new_node_id) {
		std::cout << "Device says it's alive! ID = " << (unsigned) new_node_id << std::endl;
		// Check if this is the node we are looking for.
		if (new_node_id == node_id) {
			found_node = true;
		}
	});

	std::cout << "Starting Core (connect to the driver and start the receiver thread)..." << std::endl;
	if (!core.start(busname, baudrate)) {
		std::cout << "Starting core failed." << std::endl;
		return EXIT_FAILURE;
	}

	std::cout << "Asking all devices to reset. You don't need to do that, but it makes"
		<< " sure all slaves are in a reproducible state." << std::endl;
	core.nmt.reset_all_nodes();

	// As an alternative you can request the slaves to announce
	// themselves:
	// core.nmt.discover_nodes();

	std::cout << "Giving the devices one second time to respond..." << std::endl;
	std::this_thread::sleep_for(std::chrono::seconds(1));

	// Check if the device has been detected (see the callback above).
	if (!found_node) {
		std::cout << "Node with ID " << (unsigned) node_id << " has not been found."<< std::endl;
		return EXIT_FAILURE;
	}

	std::cout << "Asking the device to start up..." << std::endl;
	core.nmt.send_nmt_message(node_id,kaco::NMT::Command::start_node);

	std::cout << "Giving the devices one second time to boot up..." << std::endl;
	std::this_thread::sleep_for(std::chrono::seconds(1));
	// ------------------------------------------------------------------------------------------------
	// Device Configuration - Motor Parameters //
	// --------------------------------------- //
	// Motor Type
//	const std::vector<uint8_t> data_mt {0x0A, 0x00};
//	core.sdo.download(node_id, 0x6402, sub_0, data_mt.size(), data_mt);
//	std::cout << "Motor Type Done!" << std::endl;
//	// Nominal Current 14900 mA (0x3A34) - Need to write it 0x34 0x3A
//	const std::vector<uint8_t> data_nc {0x40, 0x1F, 0x00, 0x00};
//	core.sdo.download(node_id, 0x3001, sub_1, data_nc.size(), data_nc);
//	std::cout << "Nominal current done! " << std::endl;
	// Output Current Limit 2 x NC, 29800 (0x7468) - for now 0x3E80	
//	const std::vector<uint8_t> data_ocl {0x80, 0x3E, 0x00, 0x00};
//	core.sdo.download(node_id, 0x3001, sub_2, data_ocl.size(), data_ocl);
//	std::cout << "Output current limit" << std::endl;
	// Number of Pole Pairs
//	const std::vector<uint8_t> data_pp {0x8};
//	core.sdo.download(node_id, 0x3001, sub_3, data_pp.size(), data_pp);
//	std::cout << "Number of pole pairs done!" << std::endl;
	// Thermal Time Constant Winding
//	const std::vector<uint8_t> data_tw {0x60, 0x00};
//	core.sdo.download(node_id, 0x3001, sub_4, data_tw.size(), data_tw);
//	std::cout << "Thermal time done!" << std::endl;
	// Torque Constant
//	const std::vector<uint8_t> data_tc {0x78, 0x82, 0x00, 0x00};
//	core.sdo.download(node_id, 0x3001, sub_5, data_tc.size(), data_tc);
//	std::cout << "Torque constant" << std::endl;
	// Max Motor Speed	
//	const std::vector<uint8_t> data_mms {0x70, 0x17, 0x00, 0x00};
//	core.sdo.download(node_id, 0x6080, sub_0, data_mms.size(), data_mms);
//	std::cout << " Max motor speed done!" << std::endl;
	// Max Motor Acceleration
//	const std::vector<uint8_t> motor_accel {0x70, 0x17, 0x00, 0x00};
//	core.sdo.download(node_id, 0x60C5, sub_0, motor_accel.size(), motor_accel);
//	std::cout << " Motor accel done!" << std::endl;
	// --------------------- //
	// Profile Velocity Mode //
	// --------------------- //
	// Modes of Operation 
	
	// Max Profile Velocity 
//	const std::vector<uint8_t> max_profile_vel{0xF8, 0x00, 0x00, 0x00};
//	core.sdo.download(node_id, 0x607F, sub_0, max_profile_vel.size(), max_profile_vel);
//	std::cout << "Max profile vel done!" <<std::endl;
	// Profile acceleration
//	const std::vector<uint8_t> profile_accel{0x32, 0x00, 0x00, 0x00};
//	core.sdo.download(node_id, 0x6083, sub_0, profile_accel.size(), profile_accel);
//	std::cout << "Profile accel done!" <<std::endl;
	// Profile decel
//	const std::vector<uint8_t> profile_decel{0x32, 0x00, 0x00, 0x00};
//	core.sdo.download(node_id, 0x6083, sub_0, profile_decel.size(), profile_decel);
//	std::cout << "Profile decel done!" << std::endl;
	// Quick Stop
//	const std::vector<uint8_t> quick_stop{0xF8, 0x00, 0x00, 0x00};
//	core.sdo.download(node_id, 0x6085, sub_0, quick_stop.size(), quick_stop);
//	std::cout << "quick stop done!" << std::endl;
	// Motion profile type - seems to only have Trapezoidal type which seems to contradict motor type
	// Check this a bit more later
//	const std::vector<uint8_t> motion_type{0x00, 0x00, 0x00, 0x00};
//	core.sdo.download(node_id, 0x6086, sub_0, motion_type.size(), motion_type};
//	std::cout << "motion type done!" << std::endl;

	// SAVE DATA //
//	const std::vector<uint8_t> data_save { 0x73, 0x61, 0x76, 0x65 };
//	core.sdo.download(node_id, ind_save, sub_1, data_save.size(), data_save);

	// -------------------------------------------------------------------------------------------------
	// Master Initialisation //	
	// --------------------- //	
	kaco::Master master;
	if(!master.start(busname, baudrate)){
		PRINT("Starting Master failed.");
		return EXIT_FAILURE;
	}

	size_t index;
	bool found = false;
	while(!found){
		for (size_t i=0; i<master.num_devices(); ++i){
			kaco::Device& device = master.get_device(i);
			device.start();
			if (device.get_device_profile_number()==402){
				index = i;
				found = true;
				PRINT("Found CiA 402 with nod ID" << device.get_node_id());
			}
		}
		std::cout << "Device with ID " << (unsigned) node_id
			<< "has not been found yet. Waiting one more sexond. Press Ctrl+C abort. " << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(1)); 
	}

	kaco::Device& device = master.get_device(index);
	std::cout << "Starting master device with ID " << (unsigned) node_id << "..." << std::endl;
	device.start();
	std::cout << "Loading EDS file." << std::endl;
	device.load_dictionary_from_eds("/home/pumba/cm-robotics/pumba/src/hw_interface/kacanopen/eds_library/CiA_profiles/maxon_motor_EPOS4.eds");
	std::cout << "------ START MASTER READING ------" << std::endl;
    	try {
        	std::cout << "Manufacturer device name = " << device.get_entry("Manufacturer device name") << std::endl;

    	} catch (const kaco::canopen_error & error) {
        	std::cout << "Getting manufacturer information failed: " << error.what() << std::endl;
    	}
    	try {        
        	std::cout << "CiA-402: Set position mode using a built-in constant (see master/src/profiles.cpp)..." << std::endl;
//        	device.set_entry("modes_of_operation", device.get_constant("profile_position_mode"));
//        	std::cout << "CiA-402: Enable operation using a built-in operation (see master/src/profiles.cpp)..." << std::endl;
        	device.execute("enable_operation");
//		device.set_entry("controlword", (uint16_t) 0x0007);
// 		device.set_entry("controlword", (uint16_t) 0x000F);
	       	std::cout << "CiA-402: Set target position..." << std::endl;
        	// Type of target position is INT32, so cast accordingly.
//        	device.execute("set_target_velocity", (int32_t) 0);
		device.set_entry(0x60FF, 0x00, (int32_t) 0);
//		device.add_transmit_pdo_mapping();
        	std::cout << "Waiting a second..." << std::endl;
        	std::this_thread::sleep_for(std::chrono::seconds(1));
        	std::cout << "CiA-402: Set target position..." << std::endl;
        	// Type of target position is INT32, so cast accordingly.
//        	device.execute("set_target_velocity", (int32_t) 50);
		device.set_entry(0x60FF, 0x00, (int32_t) 50);
        	std::cout << "Waiting a second..." << std::endl;
        	std::this_thread::sleep_for(std::chrono::seconds(1));
        	std::cout << "CiA-402: Set target position..." << std::endl;
        	// Type of target position is INT32, so cast accordingly.
//        	device.execute("set_target_velocity", (int32_t) 248);
		device.set_entry(0x60FF, 0x00, (int32_t) 248);
		std::this_thread::sleep_for(std::chrono::seconds(1));
		device.set_entry("controlword", (uint16_t) 0x0006);
    	} catch (const kaco::canopen_error & error) {
        	std::cout << "CiA-402 failed: " << error.what() << std::endl;
    	}

	std::cout << "------ END MASTER READING ------" << std::endl;
	// ----------------------------------------------------------------------------------------------- 
	// Device usage //
	// ------------ //

	std::cout << "Writing to a dictionary entry (CANopen speech: \"download\")..." << std::endl;
	const std::vector<uint8_t> data_hb {0xD0, 0x07};
	core.sdo.download(node_id, ind_hb, sub_0, data_hb.size(), data_hb);

	std::cout << "Reading the device type (\"upload\" 0x1000)... Little-endian!" << std::endl;
	std::vector<uint8_t> device_type = core.sdo.upload(node_id,0x1000,0x0);
	for (uint8_t device_type_byte : device_type) {
		std::cout << "  byte 0x" << std::hex << (unsigned) device_type_byte;// << std::endl;
	}
	std::cout << " " << std::endl;
	std::cout << "Reading the device name (\"upload\" 0x1008 - usually using segmented transfer)..." << std::endl;
	std::vector<uint8_t> device_name = core.sdo.upload(node_id,0x1008,0x0);
	std::string result(reinterpret_cast<char const*>(device_name.data()), device_name.size());
	std::cout << "Device: " << result << std::endl;

	// ------------ //
	// Control Loop //
	// ------------ //
	// Control - Switch on & Enable
//	const std::vector<uint8_t> switch_on_enable{0x06, 0x00, 0x00, 0x00};
//	core.sdo.download(node_id, 0x6040, sub_0, switch_on_enable.size(), switch_on_enable);
//	std::cout << "Switched on!" << std::endl;
	
	// Main loop - speed control

	// Control - Switch off
	
	return EXIT_SUCCESS;

}
